ALL: build

SRC = utils.c irm.c pcstr.c strgmres.c strcheb.c pcprod.c pcblock.c pmres.c main.c

-include ${PETSC_DIR}/lib/petsc/conf/variables

MAINFILE := eddy
CPPFLAGS := -I./include ${PETSC_CCPPFLAGS}
CFLAGS   := ${CC_FLAGS}
SRC      := $(addprefix src/,$(SRC))
OBJ      := $(SRC:.c=.o)
LIB      := ${PETSC_TAO_LIB}


src/%.o: %.c
	@echo ${PETSC_COMPILE_SINGLE}

${MAINFILE}: ${OBJ}
	-${CLINKER} -o ${MAINFILE} ${OBJ} ${LIB} 

build: ${MAINFILE}

clean:
	-@${RM} ${OBJ}
	-@${RM} ${OBJ:.o=.d}
	-@${RM} ${MAINFILE}

.PHONY: ALL build clean

