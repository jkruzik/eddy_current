## Getting Data
To get data files for a specific level, e.g., for level2 call
```sh
$ git lfs pull --include="level2*" --exclude=""
```
To get all data files, call
```sh
$ git lfs pull --exclude=""
```
