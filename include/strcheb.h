#ifndef STRCHEB_H
#define STRCHEB_H

#include <utils.h>

PetscErrorCode strcheb(Mat,Mat,Mat,PetscScalar,PetscScalar,Vec,PetscReal,PetscInt,PetscReal,PetscBool);

#endif

