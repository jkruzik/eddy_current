#ifndef IRM_H
#define IRM_H

#include <utils.h>

PetscErrorCode irm(Mat,Mat,Mat,PetscScalar,PetscScalar,Vec,PetscReal,PetscInt,PetscReal,PetscReal,PetscBool);
PetscErrorCode irm2(Mat,Mat,Mat,PetscScalar,PetscScalar,PetscScalar,Vec,PetscReal,PetscInt,PetscReal,PetscReal,PetscBool);
PetscErrorCode irmcheb(Mat,Mat,Mat,PetscScalar,PetscScalar,Vec,PetscReal,PetscInt,PetscReal,PetscReal,PetscBool);

#endif

