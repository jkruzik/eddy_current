#ifndef STRGRMRES_H
#define STRGRMRES_H

#include <utils.h>

PetscErrorCode strgmres(Mat,Mat,Mat,PetscScalar,PetscScalar,Vec,PetscReal,PetscInt,PetscReal,PetscBool);

#endif

