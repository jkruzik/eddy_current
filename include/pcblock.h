#ifndef PCBLOCK_H
#define PCBLOCK_H

#include <petscksp.h>


typedef struct {
  KSP           kspQ,kspM;
  Mat           K,M;
  Vec           rhsr,rhsi,vec;
  PetscScalar   beta,omega;
  PetscLogEvent ekspQ,ekspM;
  PetscBool     triflg;
} blockPC;

PetscErrorCode PCBlockCreate(PC,Mat,Mat,PetscScalar,PetscScalar,PetscReal,PetscReal,PetscBool,PetscInt);
PetscErrorCode PCBlockGetIts(PC,PetscInt*,PetscInt*);

#endif

