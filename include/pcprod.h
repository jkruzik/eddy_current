#ifndef PCPROD_H
#define PCPROD_H

#include <utils.h>

typedef struct {
  KSP           ksp;
  Mat           K,M;
  Vec           y;
  PetscScalar   c1,c2;
  PetscLogEvent eksp;
} MatPCProdCtx;

typedef struct {
  KSP           ksp;
  Mat           Q,M;
  Vec           y;
  PetscLogEvent eksp;
} prodPC;

PetscErrorCode MatPCProdCreate(Mat,Mat,PetscScalar,PetscScalar,PetscReal,PetscBool,Mat*);
PetscErrorCode MatPCProdGetIts(Mat,PetscInt*);
PetscErrorCode PCProdCreate(PC,Mat,Mat,PetscScalar,PetscScalar,PetscReal,PetscBool);
PetscErrorCode PCProdGetIts(PC,PetscInt*);

#endif

