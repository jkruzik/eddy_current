#ifndef PMRES_H
#define PMRES_H

#include <petscksp.h>

PetscErrorCode  pmres(Mat,Mat,PetscScalar,PetscScalar,Vec,PetscReal,PetscInt,PetscReal,PetscReal,PetscReal,PetscBool);

#endif

