#ifndef UTILS_H
#define UTILS_H

#include <petscksp.h>

PetscErrorCode InnerKSPSetUp(Mat,PetscReal,const char[],PetscBool,KSP*);
PetscErrorCode VecGetRealImaginaryPart(Vec x,Vec yr,Vec yi);

#endif

