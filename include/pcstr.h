#ifndef PCSTR_H
#define PCSTR_H

#include <petscksp.h>


typedef struct {
  KSP           ksp;
  Mat           Q,M;
  Vec           rhs,rhsr,rhsi,vec,vec2;
  PetscScalar   c1,c2;
  PetscLogEvent eksp;
} strPC;

PetscErrorCode PCStructuredCreate(PC,Mat,Mat,PetscScalar,PetscScalar,PetscReal,PetscBool,PetscBool);
PetscErrorCode PCStructuredGetIts(PC,PetscInt*);

#endif

