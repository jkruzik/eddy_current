static char help[] =  "This example compares preconditioners for time-periodic eddy current optimal control problem.\n\
Exaple usage: \n\
mpirun -n 4 ./eddy -ne 7\n\
";

#include <petscksp.h>
#include <irm.h>
#include <strgmres.h>
#include <strcheb.h>
#include <pcprod.h>
#include <pmres.h>


int main(int argc,char **args)
{
  PetscInt           itm,itq,its,maxits=100;
  PetscReal          eps_Q=1e-3,eps_M=1e-6,eps_outer=1e-6;
  PetscReal          beta=1e-2,omega=1,alpha=PetscSqrtReal(PetscSqrtReal(.5));
  PetscBool          flg,view=PETSC_FALSE;
  PetscViewer        viewer;
  Mat                K,M,Q,Qa,matprod;
  Vec                rhs,Myhat,sol;
  KSP                ksp;
  PC                 pcprod;
  KSPConvergedReason reason;
  PetscLogEvent      esetup,esolve;
  PetscLogStage      sload,sassembly,sirm,sirm2,sirmcg,scheb,sstrcheb;
  char               filename[4][PETSC_MAX_PATH_LEN];
  PetscErrorCode     ierr;
 
  /* Init PETSc */
  ierr = PetscInitialize(&argc,&args,(char*)0,help); if (ierr) return ierr;
  /* Params */
  ierr = PetscOptionsGetReal(NULL,NULL,"-outer_tol",&eps_outer,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-q_tol",&eps_Q,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-m_tol",&eps_M,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-max_it",&maxits,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetReal(NULL,NULL,"-beta",&beta,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-omega",&omega,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-alpha",&alpha,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(NULL,NULL,"-viewksps",&view,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetString(NULL,NULL,"-fk",filename[0],PETSC_MAX_PATH_LEN,&flg);CHKERRQ(ierr);
  if (!flg) {
    strcpy(filename[0],"data/level1_K");
  }
  ierr = PetscOptionsGetString(NULL,NULL,"-fm",filename[1],PETSC_MAX_PATH_LEN,&flg);CHKERRQ(ierr);
  if (!flg) {
    strcpy(filename[1],"data/level1_M");
  }
  ierr = PetscOptionsGetString(NULL,NULL,"-frhs",filename[2],PETSC_MAX_PATH_LEN,&flg);CHKERRQ(ierr);
  if (!flg) {
    strcpy(filename[2],"data/level1_rhs");
  }
  ierr = PetscOptionsGetString(NULL,NULL,"-fhat",filename[3],PETSC_MAX_PATH_LEN,&flg);CHKERRQ(ierr);
  if (!flg) {
    strcpy(filename[3],"data/level1_Myhat");
  }

  ierr = PetscLogEventRegister("Setup",KSP_CLASSID,&esetup);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Solve",KSP_CLASSID,&esolve);CHKERRQ(ierr);

  /* Load */
  ierr = PetscLogStageRegister("Load", &sload);CHKERRQ(ierr);
  ierr = PetscLogStagePush(sload);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename[0], FILE_MODE_READ, &viewer);CHKERRQ(ierr);
  ierr = MatCreate(PETSC_COMM_WORLD, &K);CHKERRQ(ierr);
  ierr = MatLoad(K, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename[1], FILE_MODE_READ, &viewer);CHKERRQ(ierr);
  ierr = MatCreate(PETSC_COMM_WORLD, &M);CHKERRQ(ierr);
  ierr = MatLoad(M, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename[2], FILE_MODE_READ, &viewer);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &rhs);CHKERRQ(ierr);
  ierr = VecLoad(rhs, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename[3], FILE_MODE_READ, &viewer);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &Myhat);CHKERRQ(ierr);
  ierr = VecLoad(Myhat, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  /* Assembly */
  ierr = PetscLogStageRegister("Assembly", &sassembly);CHKERRQ(ierr);
  ierr = PetscLogStagePush(sassembly);CHKERRQ(ierr);
  ierr = MatDuplicate(M, MAT_COPY_VALUES, &Q);CHKERRQ(ierr);
  ierr = MatScale(Q, PetscSqrtReal(1.0+beta*omega*omega));CHKERRQ(ierr);
  ierr = MatAXPY(Q, PetscSqrtReal(beta), K, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);      // Q = PetscSqrtReal(1+beta*omega^2)*M+PetscSqrtReal(beta)*K;

  ierr = MatDuplicate(M, MAT_COPY_VALUES, &Qa);CHKERRQ(ierr);
  ierr = MatScale(Qa, alpha*PetscSqrtReal(1.0+beta*omega*omega));CHKERRQ(ierr);
  ierr = MatAXPY(Qa, PetscSqrtReal(beta), K, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);      // Q = alpha*PetscSqrtReal(1+beta*omega^2)*M+PetscSqrtReal(beta)*K;
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  /* Solves*/
  ierr = strgmres(M,K,Q,beta,omega,Myhat,eps_outer,maxits,eps_Q,view);CHKERRQ(ierr);

  ierr = PetscLogStageRegister("STRCHEB", &sstrcheb);CHKERRQ(ierr);
  ierr = PetscLogStagePush(sstrcheb);CHKERRQ(ierr);
  ierr = strcheb(M,K,Q,beta,omega,Myhat,eps_outer,maxits,eps_Q,view);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  ierr = PetscLogStageRegister("IRM", &sirm);CHKERRQ(ierr);
  ierr = PetscLogStagePush(sirm);CHKERRQ(ierr);
  ierr = irm(M,K,Q,beta,omega,Myhat,eps_outer,maxits,eps_M,eps_Q,view);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  
  //ierr = PetscLogStageRegister("IRMII", &sirm2);CHKERRQ(ierr);
  //ierr = PetscLogStagePush(sirm2);CHKERRQ(ierr);
  //ierr = irm2(M,K,Qa,beta,omega,alpha,Myhat,eps_outer,maxits,eps_M,eps_Q,view);CHKERRQ(ierr);
  //ierr = PetscLogStagePop();CHKERRQ(ierr);

  ierr = PetscLogStageRegister("IRMCG", &sirmcg);CHKERRQ(ierr);
  ierr = PetscLogStagePush(sirmcg);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(esetup,0,0,0,0);CHKERRQ(ierr);
  ierr = VecDuplicate(Myhat,&sol);CHKERRQ(ierr);
  ierr = MatPCProdCreate(K,M,beta,omega,eps_M,view,&matprod);
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pcprod);CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPFCG);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,matprod,matprod);CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp,eps_outer,PETSC_DEFAULT,PETSC_DEFAULT,maxits);CHKERRQ(ierr);

  ierr = PCProdCreate(pcprod,Q,M,beta,omega,eps_Q,view);

  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSetUp(ksp);CHKERRQ(ierr);
  if (view) {
    ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = PetscLogEventEnd(esetup,0,0,0,0);CHKERRQ(ierr);

  /* Solve */
  ierr = PetscLogEventBegin(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ksp,Myhat,sol);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr);
  ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
  ierr = MatPCProdGetIts(matprod,&itm);CHKERRQ(ierr);
  ierr = PCProdGetIts(pcprod,&itq);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"IRM_CG %s in %D iteration, gamgM avg iters: %D, gamgQ avg iters: %D\n",KSPConvergedReasons[reason],its,
                      (PetscInt)PetscCeilReal((PetscReal)itm/(PetscReal)its),
                      (PetscInt)PetscCeilReal((PetscReal)itq/(PetscReal)its));CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = MatDestroy(&matprod);CHKERRQ(ierr);
  ierr = VecDestroy(&sol);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  ierr = PetscLogStageRegister("IRMCheb", &scheb);CHKERRQ(ierr);
  ierr = PetscLogStagePush(scheb);CHKERRQ(ierr);
  ierr = irmcheb(M,K,Q,beta,omega,Myhat,eps_outer,maxits,eps_M,eps_Q,view);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  ierr = pmres(M,K,beta,omega,Myhat,eps_outer,maxits,eps_outer,eps_M,eps_Q,view);CHKERRQ(ierr);

  ierr = MatDestroy(&K);CHKERRQ(ierr);
  ierr = MatDestroy(&M);CHKERRQ(ierr);
  ierr = MatDestroy(&Q);CHKERRQ(ierr);
  ierr = MatDestroy(&Qa);CHKERRQ(ierr);
  ierr = VecDestroy(&rhs);CHKERRQ(ierr);
  ierr = VecDestroy(&Myhat);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}

