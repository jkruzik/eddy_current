#include <utils.h>

PetscErrorCode InnerKSPSetUp(Mat M,PetscReal tol,const char prefix[],PetscBool view,KSP *kspinner)
{
  KSP            ksp;
  PC             pc;
  PetscErrorCode ierr;
  
  PetscFunctionBeginUser;
  ierr = KSPCreate(PetscObjectComm((PetscObject)M),&ksp);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPRICHARDSON);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCGAMG);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,M,M);CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp,tol,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,prefix);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSetUp(ksp);CHKERRQ(ierr);
  if (view) {
    ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  *kspinner = ksp;
  PetscFunctionReturn(0);
}

PetscErrorCode VecGetRealImaginaryPart(Vec x,Vec yr,Vec yi)
{
  PetscErrorCode    ierr;
  PetscInt          i,n;
  PetscScalar       *xa,*yra,*yia;

  PetscFunctionBegin;
  ierr = VecGetLocalSize(x,&n);CHKERRQ(ierr);
  ierr = VecGetArray(x,&xa);CHKERRQ(ierr);
  ierr = VecGetArray(yr,&yra);CHKERRQ(ierr);
  ierr = VecGetArray(yi,&yia);CHKERRQ(ierr);
  for (i=0; i<n; i++) {
    yra[i] = PetscRealPart(xa[i]);
    yia[i] = PetscImaginaryPart(xa[i]);
  }
  ierr = VecRestoreArray(x,&xa);CHKERRQ(ierr);
  ierr = VecRestoreArray(yr,&yra);CHKERRQ(ierr);
  ierr = VecRestoreArray(yi,&yia);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

