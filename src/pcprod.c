#include <pcprod.h>

static PetscErrorCode MatPCProdMult(Mat A,Vec x,Vec y)
{
  MatPCProdCtx   *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MatShellGetContext(A,(void**)&ctx);CHKERRQ(ierr);
  ierr = MatMult(ctx->K,x,y);CHKERRQ(ierr);                /* y  = Kx  */
  ierr = PetscLogEventBegin(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,y,ctx->y);CHKERRQ(ierr);        /* cy = M\y */
  ierr = PetscLogEventEnd(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = MatMult(ctx->K,ctx->y,y);CHKERRQ(ierr);           /* y  = Kcy */
  ierr = MatMult(ctx->M,x,ctx->y);CHKERRQ(ierr);           /* cy = Mx  */
  ierr = VecAXPBY(y,ctx->c1,ctx->c2,ctx->y);CHKERRQ(ierr); /* y = (1+beta*omega^2)*cy +beta*y */
  PetscFunctionReturn(0);
}

static PetscErrorCode MatPCProdDestroy(Mat A)
{
  MatPCProdCtx   *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MatShellGetContext(A,(void**)&ctx);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->K);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->M);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->y);CHKERRQ(ierr);
  ierr = KSPDestroy(&ctx->ksp);CHKERRQ(ierr);
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode MatPCProdGetIts(Mat A,PetscInt *its)
{
  MatPCProdCtx   *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MatShellGetContext(A,(void**)&ctx);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(ctx->ksp,its);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode MatPCProdCreate(Mat K,Mat M,PetscScalar beta,PetscScalar omega,PetscReal eps_M,PetscBool view,Mat *A)
{
  PetscInt       n,N;
  MatPCProdCtx   *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr= PetscNew(&ctx);CHKERRQ(ierr);
  ctx->K  = K;
  ctx->M  = M;
  ctx->c1 = 1.0 + beta*omega*omega;
  ctx->c2 = beta;

  ierr = PetscLogEventRegister("AGMGM",KSP_CLASSID,&ctx->eksp);CHKERRQ(ierr);
  ierr = InnerKSPSetUp(M,eps_M,"kspm_",view,&ctx->ksp);
  ierr = MatCreateVecs(M,NULL,&ctx->y);CHKERRQ(ierr);
  ierr = PetscObjectReference((PetscObject)K);CHKERRQ(ierr);
  ierr = PetscObjectReference((PetscObject)M);CHKERRQ(ierr);

  ierr = MatGetSize(M,&N,NULL);CHKERRQ(ierr);
  ierr = MatGetLocalSize(M,&n,NULL);CHKERRQ(ierr);
  ierr = MatCreateShell(PetscObjectComm((PetscObject)M),n,n,N,N,ctx,A);CHKERRQ(ierr);
  ierr = MatShellSetOperation(*A,MATOP_MULT,(void(*)(void))MatPCProdMult);CHKERRQ(ierr);
  ierr = MatShellSetOperation(*A,MATOP_DESTROY,(void(*)(void))MatPCProdDestroy);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode PCProdSetUp(PC pc,Mat Q,Mat M,PetscScalar beta,PetscScalar omega,PetscReal eps_Q,PetscBool view)
{
  prodPC         *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ctx->Q = Q;
  ctx->M = M;
  ierr = PetscObjectReference((PetscObject)Q);CHKERRQ(ierr);
  ierr = PetscObjectReference((PetscObject)M);CHKERRQ(ierr);
  ierr = MatCreateVecs(Q,NULL,&ctx->y);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGQ",KSP_CLASSID,&ctx->eksp);CHKERRQ(ierr);
  ierr = InnerKSPSetUp(Q,eps_Q,"kspq_",view,&ctx->ksp);
  PetscFunctionReturn(0);
}

static PetscErrorCode PCProdApply(PC pc,Vec f,Vec y)
{
  prodPC         *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,f,y);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = MatMult(ctx->M,y,ctx->y);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,ctx->y,y);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


static PetscErrorCode PCProdDestroy(PC pc)
{
  prodPC         *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->Q);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->M);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->y);CHKERRQ(ierr);
  ierr = KSPDestroy(&ctx->ksp);CHKERRQ(ierr);
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode PCProdGetIts(PC pc,PetscInt *its)
{
  prodPC         *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(ctx->ksp,its);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PCProdCreate(PC pc,Mat Q,Mat M,PetscScalar beta,PetscScalar omega,PetscReal eps_Q,PetscBool view)
{
  prodPC         *pcctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCSetType(pc,PCSHELL);CHKERRQ(ierr);
  ierr = PetscNew(&pcctx);CHKERRQ(ierr);
  ierr = PCShellSetContext(pc,pcctx);CHKERRQ(ierr);
  ierr = PCShellSetApply(pc,PCProdApply);CHKERRQ(ierr);
  ierr = PCShellSetDestroy(pc,PCProdDestroy);CHKERRQ(ierr);
  ierr = PCProdSetUp(pc,Q,M,beta,omega,eps_Q,view);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

