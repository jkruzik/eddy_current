#include <strcheb.h>

PetscErrorCode  strcheb(Mat M,Mat K,Mat Q,PetscScalar beta,PetscScalar omega,Vec rhs,PetscReal tol,PetscInt maxit,
                          PetscReal eps_Q,PetscBool view)
{
  PetscScalar    lmin, lmax, alp, rho, alpha1, beta1,c1,c2;
  PetscInt       it,itQ;
  PetscReal      res0,res;
  Vec            u,u0,u00,y,y1,y2,f,f1,f2,rhsnest,rhs2,rhsr,rhsi,vec,vec2;
  Mat            A,mats[4];
  KSP            kspQ;
  PetscErrorCode ierr;
  PetscLogEvent  esetup,esolve,ekspQ;

  PetscFunctionBeginUser;
  lmin = .5;     // Min eig
  lmax = 1.;     // Max eig
  alp = .5*(lmin+lmax);
  rho = (.25*(lmax-lmin))*(.25*(lmax-lmin));
  c1 = PetscSqrtReal(1.0 + beta*omega*omega);
  c2 = -PETSC_i*PetscSqrtReal(beta)*omega;

  ierr = PetscLogEventRegister("Setup",KSP_CLASSID,&esetup);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Solve",KSP_CLASSID,&esolve);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGQ",KSP_CLASSID,&ekspQ);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esetup,0,0,0,0);CHKERRQ(ierr);
  mats[0] = M;
  ierr = MatDuplicate(M,MAT_COPY_VALUES,&mats[1]);CHKERRQ(ierr);
  ierr = MatScale(mats[1],PetscSqrtReal(beta)*omega*PETSC_i);CHKERRQ(ierr);
  ierr = MatAXPY(mats[1],-PetscSqrtReal(beta),K,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
  ierr = MatDuplicate(M,MAT_COPY_VALUES,&mats[2]);CHKERRQ(ierr);
  ierr = MatScale(mats[2],PetscSqrtReal(beta)*omega*PETSC_i);CHKERRQ(ierr);
  ierr = MatAXPY(mats[2],PetscSqrtReal(beta),K,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
  mats[3] = M;
  ierr = MatCreateNest(PETSC_COMM_WORLD,2,NULL,2,NULL,mats,&A);CHKERRQ(ierr);
  ierr = MatNestSetVecType(A,VECNEST);CHKERRQ(ierr);
  ierr = MatCreateVecs(A,&u,&rhsnest);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(rhsnest,1,&rhs2);CHKERRQ(ierr);
  ierr = VecSet(rhs2,0.0);CHKERRQ(ierr);
  ierr = VecNestSetSubVec(rhsnest,0,rhs);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&u0);CHKERRQ(ierr);
  ierr = VecDuplicate(u,&u00);CHKERRQ(ierr);
  ierr = VecDuplicate(rhsnest, &f);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(f,0,&f1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(f,1,&f2);CHKERRQ(ierr);
  ierr = VecDuplicate(rhsnest,&y);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,0,&y1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,1,&y2);CHKERRQ(ierr);
  ierr = VecDuplicate(f1,&rhsr);CHKERRQ(ierr);
  ierr = VecDuplicate(f1,&rhsi);CHKERRQ(ierr);
  ierr = VecDuplicate(f1,&vec);CHKERRQ(ierr);
  ierr = VecDuplicate(f1,&vec2);CHKERRQ(ierr);

  ierr = InnerKSPSetUp(Q,eps_Q,"kspq_",view,&kspQ);
  ierr = PetscLogEventEnd(esetup,0,0,0,0);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = VecSet(u0,0.0);CHKERRQ(ierr);
  ierr = VecCopy(rhsnest,f);CHKERRQ(ierr);

  ierr = VecNorm(f, NORM_2, &res0);CHKERRQ(ierr);
  res = 1.;                             //    res = norm(y)/res0;
  it = 0;
  while (it < maxit && res > tol) {
    if (it ==0) {
      beta1 = 2./alp;
    } else {
        beta1 = 1./(alp-rho*beta1);
        alpha1 = alp*beta1;
    }
    ierr = VecWAXPY(vec,c1+c2,f1,f2);CHKERRQ(ierr); /* u = (c1 +c2)*f1 +f2 */
    ierr = VecGetRealImaginaryPart(vec,rhsr,rhsi);CHKERRQ(ierr);

    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,rhsr,vec);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,rhsi,vec2);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);

    ierr = VecAXPY(vec,PETSC_i,vec2);CHKERRQ(ierr);
    ierr = MatMult(M,vec,vec2);CHKERRQ(ierr);
    ierr = VecAXPY(vec2,-1.0,f1);CHKERRQ(ierr);
    ierr = VecGetRealImaginaryPart(vec2,rhsr,rhsi);CHKERRQ(ierr);

    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,rhsr,y2);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,rhsi,vec2);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = VecAXPY(y2,PETSC_i,vec2);CHKERRQ(ierr);
    ierr = VecWAXPY(y1,-c1+c2,y2,vec);CHKERRQ(ierr); /* y1 = vec + (-c1 +c2)*y2 */

    if (it == 0) {
      ierr = VecWAXPY(u, .5*beta1, y, u0);CHKERRQ(ierr);    //     u = u0+(beta1/2)*x;
    } else {
      ierr = VecWAXPY(u, alpha1, u0, u00);CHKERRQ(ierr);
      ierr = VecAXPY(u, -alpha1, u00);CHKERRQ(ierr);
      ierr = VecAXPY(u, beta1, y);CHKERRQ(ierr);           //    u = alpha1*u0+(1-alpha1)*u00+beta1*x;
    }

    ierr = MatMult(A,u,f);CHKERRQ(ierr);                   //      f = A*u;
    ierr = VecAYPX(f,-1.0,rhsnest);CHKERRQ(ierr);              //     f = rhs-f;

    it = it+1;
    ierr = VecNorm(f, NORM_2, &res);CHKERRQ(ierr); res = res/res0;    //     res = norm(y)/res0;
    ierr = VecCopy(u0, u00);CHKERRQ(ierr);                    //     u00 = u0;
    ierr = VecCopy(u, u0);CHKERRQ(ierr);                      //     u0 = u;
  }
  ierr = PetscLogEventEnd(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(kspQ,&itQ);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"STRCHEB in %d iteration, rel res: %e, gamgQ avg iters: %d\n",it,res,
                     (PetscInt)PetscCeilReal((PetscReal)itQ/(PetscReal)it));CHKERRQ(ierr);

  ierr = KSPDestroy(&kspQ);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&u0);CHKERRQ(ierr);
  ierr = VecDestroy(&u00);CHKERRQ(ierr);
  ierr = VecDestroy(&rhsnest);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  ierr = VecDestroy(&f);CHKERRQ(ierr);
  ierr = VecDestroy(&rhsr);CHKERRQ(ierr);
  ierr = VecDestroy(&rhsi);CHKERRQ(ierr);
  ierr = VecDestroy(&vec);CHKERRQ(ierr);
  ierr = VecDestroy(&vec2);CHKERRQ(ierr);
  ierr = MatDestroy(&mats[1]);CHKERRQ(ierr);
  ierr = MatDestroy(&mats[2]);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

