#include <irm.h>

PetscErrorCode irm(Mat M,Mat K,Mat Q,PetscScalar beta,PetscScalar omega,Vec rhs,PetscReal tol,PetscInt maxit,
                   PetscReal eps_M,PetscReal eps_Q,PetscBool view)
{
  PetscScalar    tau=4./3.;
  PetscInt       it,itM,itQ;
  PetscReal      res0,res;
  Vec            x,u,y,y1,y2,Mu,Ky2;
  KSP            kspM,kspQ;
  PetscErrorCode ierr;
  PetscLogEvent  esetup,esolve,ekspM,ekspQ;

  PetscFunctionBeginUser;
  ierr = MatCreateVecs(M,&u,NULL);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y1);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &Mu);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y2);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &Ky2);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &x);CHKERRQ(ierr);

  ierr = PetscLogEventRegister("Setup",KSP_CLASSID,&esetup);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Solve",KSP_CLASSID,&esolve);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGM",KSP_CLASSID,&ekspM);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGQ",KSP_CLASSID,&ekspQ);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esetup,0,0,0,0);CHKERRQ(ierr);
  ierr = InnerKSPSetUp(M,eps_M,"kspm_",view,&kspM);
  ierr = InnerKSPSetUp(Q,eps_Q,"kspq_",view,&kspQ);
  ierr = PetscLogEventEnd(esetup,0,0,0,0);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = VecSet(u,0.0);CHKERRQ(ierr);

  ierr = MatMult(K, u, y1);CHKERRQ(ierr);  //     y1 = K*u;

  ierr = PetscLogEventBegin(ekspM,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(kspM,y1,y2);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ekspM,0,0,0,0);CHKERRQ(ierr);

  ierr = MatMult(M, u, Mu);CHKERRQ(ierr);
  ierr = VecWAXPY(y, -1.0-beta*omega*omega, Mu, rhs);CHKERRQ(ierr);
  ierr = MatMult(K, y2, Ky2);CHKERRQ(ierr);
  ierr = VecAXPY(y, -beta, Ky2);CHKERRQ(ierr); //     y = rhs-(1+beta*omega^2)*M*u-beta*K*y2;

  ierr = VecNorm(y, NORM_2, &res0);CHKERRQ(ierr);
  res = 1.;
  it = 0;
  while (it < maxit && res > tol) {
    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,y,y1);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = MatMult(M, y1, y2);CHKERRQ(ierr);       //    y2 = M*y1;
    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,y2,x);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);

    ierr = VecAXPY(u,tau,x);CHKERRQ(ierr);    //     u = u0+tau*x;

    ierr = MatMult(K, u, y1);CHKERRQ(ierr);                   //      y1 = K*u;
    ierr = PetscLogEventBegin(ekspM,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspM,y1,y2);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspM,0,0,0,0);CHKERRQ(ierr);

    ierr = MatMult(M, u, Mu);CHKERRQ(ierr);
    ierr = VecWAXPY(y, -1.0-beta*omega*omega, Mu, rhs);CHKERRQ(ierr);
    ierr = MatMult(K, y2, Ky2);CHKERRQ(ierr);
    ierr = VecAXPY(y, -beta, Ky2);CHKERRQ(ierr);              //     y = rhs-(1+beta*omega^2)*M*u-beta*K*y2;

    it = it+1;
    ierr = VecNorm(y, NORM_2, &res);CHKERRQ(ierr); res = res/res0;    //     res = norm(y)/res0;
  }
  ierr = PetscLogEventEnd(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(kspM,&itM);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(kspQ,&itQ);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"IRM iters: %d, rel res: %e, avg gamgM iter: %d, avg gamgQ iter: %d\n",it,res,
                     (PetscInt)PetscCeilReal((PetscReal)itM/(PetscReal)it),(PetscInt)PetscCeilReal((PetscReal)itQ/(PetscReal)it));CHKERRQ(ierr);

  ierr = KSPDestroy(&kspM);CHKERRQ(ierr);
  ierr = KSPDestroy(&kspQ);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&y1);CHKERRQ(ierr);
  ierr = VecDestroy(&Mu);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  ierr = VecDestroy(&y2);CHKERRQ(ierr);
  ierr = VecDestroy(&Ky2);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode irm2(Mat M,Mat K,Mat Q,PetscScalar beta,PetscScalar omega,PetscScalar alpha,Vec rhs,PetscReal tol,PetscInt maxit,
                   PetscReal eps_M,PetscReal eps_Q,PetscBool view)
{
  PetscInt       it,itQ,itM;
  PetscReal      res0,res;
  Vec            u,y,y1,y2,Mu,Ky2;
  KSP            kspM,kspQ;
  PetscErrorCode ierr;
  PetscLogEvent  esetup,esolve,ekspM,ekspQ;

  PetscFunctionBeginUser;
  ierr = MatCreateVecs(M,&u,NULL);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y1);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &Mu);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y2);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &Ky2);CHKERRQ(ierr);

  ierr = PetscLogEventRegister("Setup",KSP_CLASSID,&esetup);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Solve",KSP_CLASSID,&esolve);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGM",KSP_CLASSID,&ekspM);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGQ",KSP_CLASSID,&ekspQ);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esetup,0,0,0,0);CHKERRQ(ierr);
  ierr = InnerKSPSetUp(M,eps_M,"kspm_",view,&kspM);
  ierr = InnerKSPSetUp(Q,eps_Q,"kspq_",view,&kspQ);
  ierr = PetscLogEventEnd(esetup,0,0,0,0);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = VecSet(u,0.0);CHKERRQ(ierr);

  ierr = MatMult(K, u, y1);CHKERRQ(ierr);  //     y1 = K*u;

  ierr = PetscLogEventBegin(ekspM,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(kspM,y1,y2);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ekspM,0,0,0,0);CHKERRQ(ierr);
  ierr = MatMult(M, u, Mu);CHKERRQ(ierr); // Mu = M*u
  ierr = VecScale(Mu,1.0+beta*omega*omega);CHKERRQ(ierr);
  ierr = VecWAXPY(y,-1.0,Mu,rhs);CHKERRQ(ierr);
  ierr = MatMult(K, y2, Ky2);CHKERRQ(ierr);
  ierr = VecAXPY(y, -beta, Ky2);CHKERRQ(ierr); //     y = rhs-(1+beta*omega^2)*M*u-beta*K*y2;

  ierr = VecNorm(y, NORM_2, &res0);CHKERRQ(ierr);
  res = 1.;
  it = 0;
  while (it < maxit && res > tol) {
    ierr = VecScale(y1,2.0*alpha*PetscSqrtReal(beta+beta*beta*omega*omega));CHKERRQ(ierr);
    ierr = VecAXPY(y1,alpha*alpha-1.0,Mu);CHKERRQ(ierr);
    ierr = VecAXPY(y1,1.0,rhs);CHKERRQ(ierr);
    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,y1,y);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = MatMult(M, y, y2);CHKERRQ(ierr);       //    y2 = M*y;
    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,y2,u);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);

    ierr = MatMult(K, u, y1);CHKERRQ(ierr);                   //      y1 = K*u;
    ierr = PetscLogEventBegin(ekspM,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspM,y1,y2);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspM,0,0,0,0);CHKERRQ(ierr);
    ierr = MatMult(M, u, Mu);CHKERRQ(ierr); // Mu = M*u
    ierr = VecScale(Mu,1.0+beta*omega*omega);CHKERRQ(ierr);
    ierr = VecWAXPY(y,-1.0,Mu,rhs);CHKERRQ(ierr);
    ierr = MatMult(K, y2, Ky2);CHKERRQ(ierr);
    ierr = VecAXPY(y, -beta, Ky2);CHKERRQ(ierr); //     y = rhs-(1+beta*omega^2)*M*u-beta*K*y2;

    it = it+1;
    ierr = VecNorm(y, NORM_2, &res);CHKERRQ(ierr); res = res/res0;    //     res = norm(y)/res0;
  }
  ierr = PetscLogEventEnd(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(kspM,&itM);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(kspQ,&itQ);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"IRM II iters: %d, rel res: %e, avg gamgM iter: %d, avg gamgQ iter: %d\n",it,res,
                     (PetscInt)PetscCeilReal((PetscReal)itM/(PetscReal)it),(PetscInt)PetscCeilReal((PetscReal)itQ/(PetscReal)it));CHKERRQ(ierr);

  ierr = KSPDestroy(&kspM);CHKERRQ(ierr);
  ierr = KSPDestroy(&kspQ);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&y1);CHKERRQ(ierr);
  ierr = VecDestroy(&Mu);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  ierr = VecDestroy(&y2);CHKERRQ(ierr);
  ierr = VecDestroy(&Ky2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode  irmcheb(Mat M,Mat K,Mat Q,PetscScalar beta,PetscScalar omega,Vec rhs,PetscReal tol,PetscInt maxit,
                          PetscReal eps_M,PetscReal eps_Q,PetscBool view)
{
  PetscScalar    lmin, lmax, alp, rho, alpha1, beta1;
  PetscInt       it,itQ,itM;
  PetscReal      res0,res;
  Vec            x,u,u0,u00,y,y1,y2,Mu,Ky2;
  KSP            kspM,kspQ;
  PetscErrorCode ierr;
  PetscLogEvent  esetup,esolve,ekspM,ekspQ;

  PetscFunctionBeginUser;
  lmin = .5;     // Min eig
  lmax = 1.;     // Max eig
  alp = .5*(lmin+lmax);
  rho = (.25*(lmax-lmin))*(.25*(lmax-lmin));

  ierr = MatCreateVecs(M,&u,NULL);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &u0);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &u00);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y1);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &Mu);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &y2);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &Ky2);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &x);CHKERRQ(ierr);

  ierr = PetscLogEventRegister("Setup",KSP_CLASSID,&esetup);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Solve",KSP_CLASSID,&esolve);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGM",KSP_CLASSID,&ekspM);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGQ",KSP_CLASSID,&ekspQ);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esetup,0,0,0,0);CHKERRQ(ierr);
  ierr = InnerKSPSetUp(M,eps_M,"kspm_",view,&kspM);
  ierr = InnerKSPSetUp(Q,eps_Q,"kspq_",view,&kspQ);
  ierr = PetscLogEventEnd(esetup,0,0,0,0);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = VecSet(u,0.0);CHKERRQ(ierr);
  ierr = VecCopy(u, u0);CHKERRQ(ierr);     //    u0 =u; % b=[p;q];

  ierr = MatMult(K, u, y1);CHKERRQ(ierr);  //     y1 = K*u;

  ierr = PetscLogEventBegin(ekspM,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(kspM,y1,y2);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ekspM,0,0,0,0);CHKERRQ(ierr);

  ierr = MatMult(M, u, Mu);CHKERRQ(ierr);
  ierr = VecWAXPY(y, -1.0-beta*omega*omega, Mu, rhs);CHKERRQ(ierr);
  ierr = MatMult(K, y2, Ky2);CHKERRQ(ierr);
  ierr = VecAXPY(y, -beta, Ky2);CHKERRQ(ierr); //     y = rhs-(1+beta*omega^2)*M*u-beta*K*y2;

  ierr = VecNorm(y, NORM_2, &res0);CHKERRQ(ierr);
  res = 1.;                             //    res = norm(y)/res0;
  it = 0;
  while (it < maxit && res > tol) {
    if (it ==0) {
      beta1 = 2./alp;
  } else {
      beta1 = 1./(alp-rho*beta1);
      alpha1 = alp*beta1;
  }

    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,y,y1);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = MatMult(M, y1, y2);CHKERRQ(ierr);       //    y2 = M*y1;
    ierr = PetscLogEventBegin(ekspQ,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspQ,y2,x);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspQ,0,0,0,0);CHKERRQ(ierr);

    if (it == 0) {
      ierr = VecWAXPY(u, .5*beta1, x, u0);CHKERRQ(ierr);    //     u = u0+(beta1/2)*x;
    } else {
      ierr = VecWAXPY(u, alpha1, u0, u00);CHKERRQ(ierr);
      ierr = VecAXPY(u, -alpha1, u00);CHKERRQ(ierr);
      ierr = VecAXPY(u, beta1, x);CHKERRQ(ierr);           //    u = alpha1*u0+(1-alpha1)*u00+beta1*x;
    }

    ierr = MatMult(K, u, y1);CHKERRQ(ierr);                   //      y1 = K*u;
    ierr = PetscLogEventBegin(ekspM,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(kspM,y1,y2);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(ekspM,0,0,0,0);CHKERRQ(ierr);

    ierr = MatMult(M, u, Mu);CHKERRQ(ierr);
    ierr = VecWAXPY(y, -1.0-beta*omega*omega, Mu, rhs);CHKERRQ(ierr);
    ierr = MatMult(K, y2, Ky2);CHKERRQ(ierr);
    ierr = VecAXPY(y, -beta, Ky2);CHKERRQ(ierr);              //     y = rhs-(1+beta*omega^2)*M*u-beta*K*y2;

    it = it+1;
    ierr = VecNorm(y, NORM_2, &res);CHKERRQ(ierr); res = res/res0;    //     res = norm(y)/res0;
    ierr = VecCopy(u0, u00);CHKERRQ(ierr);                    //     u00 = u0;
    ierr = VecCopy(u, u0);CHKERRQ(ierr);                      //     u0 = u;
  }
  ierr = PetscLogEventEnd(esolve,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(kspM,&itM);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(kspQ,&itQ);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"IRM_CHEB iters: %d, rel res: %e, avg gamgM iter: %d, avg gamgQ iter: %d\n",it,res,
                     (PetscInt)PetscCeilReal((PetscReal)itM/(PetscReal)it),(PetscInt)PetscCeilReal((PetscReal)itQ/(PetscReal)it));CHKERRQ(ierr);

  ierr = KSPDestroy(&kspM);CHKERRQ(ierr);
  ierr = KSPDestroy(&kspQ);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&u0);CHKERRQ(ierr);
  ierr = VecDestroy(&u00);CHKERRQ(ierr);
  ierr = VecDestroy(&y1);CHKERRQ(ierr);
  ierr = VecDestroy(&Mu);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  ierr = VecDestroy(&y2);CHKERRQ(ierr);
  ierr = VecDestroy(&Ky2);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

