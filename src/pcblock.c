#include <pcblock.h>
#include <utils.h>

static PetscErrorCode BlockPCCreate(blockPC **ctx)
{
  blockPC        *newctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr= PetscNew(&newctx);CHKERRQ(ierr);
  *ctx = newctx;
  PetscFunctionReturn(0);
}

static PetscErrorCode BlockPCSetUp(PC pc,Mat K,Mat M,PetscScalar beta,PetscScalar omega,PetscReal eps_M,PetscReal eps_Q,PetscBool view,PetscInt variant)
{
  blockPC        *ctx;
  Mat            Q;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ctx->triflg = PETSC_FALSE;
  ctx->K      = K;
  ctx->M      = M;
  ctx->beta   = beta;
  ctx->omega  = omega;
  ierr = PetscObjectReference((PetscObject)K);CHKERRQ(ierr);
  ierr = PetscObjectReference((PetscObject)M);CHKERRQ(ierr);
  if (!variant) {
    ierr = MatDuplicate(M,MAT_COPY_VALUES,&Q);CHKERRQ(ierr);
    ierr = MatScale(Q,1.0+PetscSqrtReal(beta)*omega);CHKERRQ(ierr);
    ierr = MatAXPY(Q,PetscSqrtReal(beta),K,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);      // Q = (1+PetscSqrtReal(beta)*omega)*M+PetscSqrtReal(beta)*K;
  } else {
    if (variant != 1) {
      ierr = InnerKSPSetUp(M,eps_M,"kspm_",view,&ctx->kspM);CHKERRQ(ierr);
      if (variant == 3) ctx->triflg = PETSC_TRUE;
    }
    ierr = MatDuplicate(M,MAT_COPY_VALUES,&Q);CHKERRQ(ierr);
    ierr = MatScale(Q, PetscSqrtReal(1.0+beta*omega*omega));CHKERRQ(ierr);
    ierr = MatAXPY(Q, PetscSqrtReal(beta),K,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);      // Q = PetscSqrtReal(1+beta*omega^2)*M+PetscSqrtReal(beta)*K;
  }
  ierr = MatCreateVecs(Q,&ctx->vec,NULL);CHKERRQ(ierr);
  //if (!cmplx) {
    ierr = VecDuplicate(ctx->vec,&ctx->rhsr);CHKERRQ(ierr);
    ierr = VecDuplicate(ctx->vec,&ctx->rhsi);CHKERRQ(ierr);
  //}
  ierr = PetscLogEventRegister("AGMGQ",KSP_CLASSID,&ctx->ekspQ);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("AGMGM",KSP_CLASSID,&ctx->ekspM);CHKERRQ(ierr);

  ierr = InnerKSPSetUp(Q,eps_Q,"kspq_",view,&ctx->kspQ);CHKERRQ(ierr);
  ierr = MatDestroy(&Q);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode VecGetRealImaginaryPart_Private(Vec x,Vec yr,Vec yi)
{
  PetscErrorCode    ierr;
  PetscInt          i,n;
  PetscScalar       *xa,*yra,*yia;

  PetscFunctionBeginUser;
  ierr = VecGetLocalSize(x,&n);CHKERRQ(ierr);
  ierr = VecGetArray(x,&xa);CHKERRQ(ierr);
  ierr = VecGetArray(yr,&yra);CHKERRQ(ierr);
  ierr = VecGetArray(yi,&yia);CHKERRQ(ierr);
  for (i=0; i<n; i++) {
    yra[i] = PetscRealPart(xa[i]);
    yia[i] = PetscImaginaryPart(xa[i]);
  }
  ierr = VecRestoreArray(x,&xa);CHKERRQ(ierr);
  ierr = VecRestoreArray(yr,&yra);CHKERRQ(ierr);
  ierr = VecRestoreArray(yi,&yia);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode BlockPCApply_BD(PC pc,Vec f,Vec y)
{
  blockPC        *ctx;
  Vec            f1,f2,y1,y2;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = VecNestGetSubVec(f,0,&f1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(f,1,&f2);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,0,&y1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,1,&y2);CHKERRQ(ierr);
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);

  ierr = VecGetRealImaginaryPart_Private(f1,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsr,y1);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsi,ctx->vec);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = VecAXPY(y1,PETSC_i,ctx->vec);CHKERRQ(ierr);

  ierr = VecGetRealImaginaryPart_Private(f2,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsr,y2);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsi,ctx->vec);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = VecAXPY(y2,PETSC_i,ctx->vec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode BlockPCApply_BT(PC pc,Vec f,Vec y)
{
  blockPC        *ctx;
  Vec            f1,f2,y1,y2;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = VecNestGetSubVec(f,0,&f1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(f,1,&f2);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,0,&y1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,1,&y2);CHKERRQ(ierr);
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);

  ierr = VecGetRealImaginaryPart_Private(f1,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(ctx->ekspM,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspM,ctx->rhsr,y1);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspM,ctx->rhsi,ctx->vec);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->ekspM,0,0,0,0);CHKERRQ(ierr);
  ierr = VecAXPY(y1,PETSC_i,ctx->vec);CHKERRQ(ierr);
  
  if (ctx->triflg) {
    ierr = MatMult(ctx->M,y1,ctx->vec);CHKERRQ(ierr);
    ierr = MatMult(ctx->K,y1,y2);CHKERRQ(ierr);
    ierr = VecAXPY(y2,PETSC_i*ctx->omega,ctx->vec);CHKERRQ(ierr);
    ierr = VecScale(y2,PetscSqrtReal(ctx->beta));CHKERRQ(ierr);
    ierr = VecAXPY(y2,-1.0,f2);CHKERRQ(ierr);
    ierr = VecGetRealImaginaryPart_Private(y2,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);
  } else {
    ierr = VecGetRealImaginaryPart_Private(f2,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);
  }
  ierr = PetscLogEventBegin(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsr,y2);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsi,ctx->vec);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = VecAXPY(y2,PETSC_i,ctx->vec);CHKERRQ(ierr);

  ierr = MatMult(ctx->M,y2,ctx->vec);CHKERRQ(ierr);

  ierr = VecGetRealImaginaryPart_Private(ctx->vec,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsr,y2);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->kspQ,ctx->rhsi,ctx->vec);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->ekspQ,0,0,0,0);CHKERRQ(ierr);
  ierr = VecAXPY(y2,PETSC_i,ctx->vec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


static PetscErrorCode BlockPCDestroy(PC pc)
{
  blockPC        *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->K);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->M);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->rhsr);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->rhsi);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->vec);CHKERRQ(ierr);
  ierr = KSPDestroy(&ctx->kspM);CHKERRQ(ierr);
  ierr = KSPDestroy(&ctx->kspQ);CHKERRQ(ierr);
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode PCBlockGetIts(PC pc,PetscInt *itsM,PetscInt* itsQ)
{
  blockPC        *ctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  if (itsM) {
    ierr = KSPGetTotalIterations(ctx->kspM,itsM);CHKERRQ(ierr);
  }
  ierr = KSPGetTotalIterations(ctx->kspQ,itsQ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PCBlockCreate(PC pc,Mat K,Mat M,PetscScalar beta,PetscScalar omega,PetscReal eps_M,PetscReal eps_Q,PetscBool view,PetscInt variant)
{
  blockPC        *pcctx;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PCSetType(pc,PCSHELL);CHKERRQ(ierr);
  ierr = BlockPCCreate(&pcctx);CHKERRQ(ierr);
  ierr = PCShellSetContext(pc,pcctx);CHKERRQ(ierr);
  if (variant==0 || variant==1) {
    ierr = PCShellSetApply(pc,BlockPCApply_BD);CHKERRQ(ierr);
  } else if (variant==2 || variant==3) {
    ierr = PCShellSetApply(pc,BlockPCApply_BT);CHKERRQ(ierr);
  } else {
      SETERRQ1(PetscObjectComm((PetscObject)M),PETSC_ERR_ARG_OUTOFRANGE,"Wrong variant %D",variant);
  }
  ierr = PCShellSetDestroy(pc,BlockPCDestroy);CHKERRQ(ierr);
  ierr = BlockPCSetUp(pc,K,M,beta,omega,eps_M,eps_Q,view,variant);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

