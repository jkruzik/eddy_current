#include <pcstr.h>
#include <utils.h>

static PetscErrorCode StructuredPCCreate(strPC **ctx)
{
  strPC          *newctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr= PetscNew(&newctx);CHKERRQ(ierr);
  *ctx = newctx;
  PetscFunctionReturn(0);
}

static PetscErrorCode StructuredPCSetUp(PC pc,Mat Q,Mat M,PetscScalar beta,PetscScalar omega,PetscReal eps_Q,PetscBool view,PetscBool cmplx)
{
  strPC          *ctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ctx->Q     = Q;
  ctx->M     = M;
  ctx->c1    = PetscSqrtReal(1.0 + beta*omega*omega);
  ctx->c2    = -PETSC_i*PetscSqrtReal(beta)*omega;
  ierr = PetscObjectReference((PetscObject)Q);CHKERRQ(ierr);
  ierr = PetscObjectReference((PetscObject)M);CHKERRQ(ierr);
  ierr = MatCreateVecs(Q,&ctx->vec,&ctx->rhs);CHKERRQ(ierr);
  if (!cmplx) {
    ierr = VecDuplicate(ctx->vec,&ctx->vec2);CHKERRQ(ierr);
    ierr = VecDuplicate(ctx->rhs,&ctx->rhsr);CHKERRQ(ierr);
    ierr = VecDuplicate(ctx->rhs,&ctx->rhsi);CHKERRQ(ierr);
  }
  ierr = PetscLogEventRegister("AGMGQ",KSP_CLASSID,&ctx->eksp);CHKERRQ(ierr);
  ierr = InnerKSPSetUp(Q,eps_Q,"kspq_",view,&ctx->ksp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode StructuredPCApplyComplex(PC pc,Vec f,Vec y)
{
  strPC          *ctx;
  Vec            f1,f2,y1,y2;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecNestGetSubVec(f,0,&f1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(f,1,&f2);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,0,&y1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,1,&y2);CHKERRQ(ierr);
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);

  ierr = VecWAXPY(ctx->rhs,ctx->c1+ctx->c2,f1,f2);CHKERRQ(ierr); /* rhs = (c1 +c2)*f1 +f2 */
  ierr = PetscLogEventBegin(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,ctx->rhs,ctx->vec);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = MatMult(ctx->M,ctx->vec,ctx->rhs);CHKERRQ(ierr);
  ierr = VecAXPY(ctx->rhs,-1.,f1);CHKERRQ(ierr); /* rhs = M*vec -f1 */
  ierr = PetscLogEventBegin(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,ctx->rhs,y2);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = VecWAXPY(y1,-ctx->c1+ctx->c2,y2,ctx->vec);CHKERRQ(ierr); /* y1 = vec + (-c1 +c2)*y2 */
  PetscFunctionReturn(0);
}

static PetscErrorCode StructuredPCApply(PC pc,Vec f,Vec y)
{
  strPC          *ctx;
  Vec            f1,f2,y1,y2;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecNestGetSubVec(f,0,&f1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(f,1,&f2);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,0,&y1);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(y,1,&y2);CHKERRQ(ierr);
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);

  ierr = VecWAXPY(ctx->rhs,ctx->c1+ctx->c2,f1,f2);CHKERRQ(ierr); /* rhs = (c1 +c2)*f1 +f2 */
  ierr = VecGetRealImaginaryPart(ctx->rhs,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,ctx->rhsr,ctx->vec);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,ctx->rhsi,ctx->vec2);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->eksp,0,0,0,0);CHKERRQ(ierr);

  ierr = VecAXPY(ctx->vec,PETSC_i,ctx->vec2);CHKERRQ(ierr);
  ierr = MatMult(ctx->M,ctx->vec,ctx->rhs);CHKERRQ(ierr);
  ierr = VecAXPY(ctx->rhs,-1.,f1);CHKERRQ(ierr); /* rhs = M*vec -f1 */
  ierr = VecGetRealImaginaryPart(ctx->rhs,ctx->rhsr,ctx->rhsi);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,ctx->rhsr,y2);CHKERRQ(ierr);
  ierr = KSPSolve(ctx->ksp,ctx->rhsi,ctx->vec2);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(ctx->eksp,0,0,0,0);CHKERRQ(ierr);
  ierr = VecAXPY(y2,PETSC_i,ctx->vec2);CHKERRQ(ierr);
  ierr = VecWAXPY(y1,-ctx->c1+ctx->c2,y2,ctx->vec);CHKERRQ(ierr); /* y1 = vec + (-c1 +c2)*y2 */
  PetscFunctionReturn(0);
}


static PetscErrorCode StructuredPCDestroy(PC pc)
{
  strPC          *ctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->Q);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->M);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->rhs);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->rhsr);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->rhsi);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->vec);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->vec2);CHKERRQ(ierr);
  ierr = KSPDestroy(&ctx->ksp);CHKERRQ(ierr);
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode PCStructuredGetIts(PC pc,PetscInt *its)
{
  strPC          *ctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ierr = KSPGetTotalIterations(ctx->ksp,its);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PCStructuredCreate(PC pc,Mat Q,Mat M,PetscScalar beta,PetscScalar omega,PetscReal eps_Q,PetscBool view,PetscBool cmplx)
{
  strPC          *pcctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PCSetType(pc,PCSHELL);CHKERRQ(ierr);
  ierr = StructuredPCCreate(&pcctx);CHKERRQ(ierr);
  ierr = PCShellSetContext(pc,pcctx);CHKERRQ(ierr);
  if (cmplx) {
    ierr = PCShellSetApply(pc,StructuredPCApplyComplex);CHKERRQ(ierr);
  } else {
    ierr = PCShellSetApply(pc,StructuredPCApply);CHKERRQ(ierr);
  }
  ierr = PCShellSetDestroy(pc,StructuredPCDestroy);CHKERRQ(ierr);
  ierr = StructuredPCSetUp(pc,Q,M,beta,omega,eps_Q,view,cmplx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

