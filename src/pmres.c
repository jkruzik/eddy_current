#include <pmres.h>
#include <pcblock.h>

PetscErrorCode  pmres(Mat M, Mat K,PetscScalar beta,PetscScalar omega,Vec rhs,PetscReal tol,PetscInt maxits,
                         PetscReal eps_outer,PetscReal eps_M,PetscReal eps_Q,PetscBool view)
{
  Mat                A,mats[4];
  Vec                rhs2,rhsnest,sol;
  KSP                ksp;
  PC                 pc;
  PetscInt           itm,itq,its,variant;
  KSPConvergedReason reason;
  PetscLogEvent      esetup,esolve;
  PetscLogStage      stage,stages[4];
  const char         *algs[]={"BD1","BD2","BD","BT"};
  PetscErrorCode     ierr;

  PetscFunctionBeginUser;
  ierr = PetscLogEventRegister("Setup",KSP_CLASSID,&esetup);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Solve",KSP_CLASSID,&esolve);CHKERRQ(ierr);
  ierr = PetscLogStageRegister("PMRES",&stage);CHKERRQ(ierr);

  ierr = PetscLogStagePush(stage);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(esetup,0,0,0,0);CHKERRQ(ierr);
  mats[0] = M;
  ierr = MatDuplicate(M,MAT_COPY_VALUES,&mats[1]);CHKERRQ(ierr);
  ierr = MatScale(mats[1],-PetscSqrtReal(beta)*omega*PETSC_i);CHKERRQ(ierr);
  ierr = MatAXPY(mats[1],PetscSqrtReal(beta),K,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
  ierr = MatDuplicate(M,MAT_COPY_VALUES,&mats[2]);CHKERRQ(ierr);
  ierr = MatScale(mats[2],PetscSqrtReal(beta)*omega*PETSC_i);CHKERRQ(ierr);
  ierr = MatAXPY(mats[2],PetscSqrtReal(beta),K,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
  ierr = MatDuplicate(M,MAT_COPY_VALUES,&mats[3]);CHKERRQ(ierr);
  ierr = MatScale(mats[3],-1.0);CHKERRQ(ierr);
  ierr = MatCreateNest(PETSC_COMM_WORLD,2,NULL,2,NULL,mats,&A);CHKERRQ(ierr);
  ierr = MatNestSetVecType(A,VECNEST);CHKERRQ(ierr);
  ierr = MatCreateVecs(A,&sol,&rhsnest);CHKERRQ(ierr);
  ierr = VecNestGetSubVec(rhsnest,1,&rhs2);CHKERRQ(ierr);
  ierr = VecSet(rhs2,0.0);CHKERRQ(ierr);
  ierr = VecNestSetSubVec(rhsnest,0,rhs);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(esetup,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  for (variant=0; variant<4; variant++) {
    ierr = PetscLogStageRegister(algs[variant],&stages[variant]);CHKERRQ(ierr);
    ierr = PetscLogStagePush(stages[variant]);CHKERRQ(ierr);
    ierr = PetscLogEventBegin(esetup,0,0,0,0);CHKERRQ(ierr);
    /* create and customize KSP */
    ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    //if (variant == 3) {
      ierr = KSPSetType(ksp,KSPFGMRES);CHKERRQ(ierr);
    //} else {
    //  ierr = KSPSetType(ksp,KSPMINRES);CHKERRQ(ierr);
    //}
    ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,eps_outer,PETSC_DEFAULT,PETSC_DEFAULT,maxits);CHKERRQ(ierr);

    ierr = PCBlockCreate(pc,K,M,beta,omega,eps_M,eps_Q,view,variant);CHKERRQ(ierr);

    ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
    ierr = KSPSetUp(ksp);CHKERRQ(ierr);
    if (view) {
      ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    }
    ierr = PetscLogEventEnd(esetup,0,0,0,0);CHKERRQ(ierr);

    /* Solve */
    ierr = PetscLogEventBegin(esolve,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPSolve(ksp,rhsnest,sol);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(esolve,0,0,0,0);CHKERRQ(ierr);
    ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr);
    ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
    if ( variant<2 ) {
      ierr = PCBlockGetIts(pc,NULL,&itq);CHKERRQ(ierr);
      itm  = 0;
    } else {
      ierr = PCBlockGetIts(pc,&itm,&itq);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(PETSC_COMM_WORLD,"%s %s in %D iteration, gamgM avg iters: %D gamgQ avg iters: %D\n",
                        algs[variant],KSPConvergedReasons[reason],its,
                        (PetscInt)PetscCeilReal((PetscReal)itm/(PetscReal)its),
                        (PetscInt)PetscCeilReal((PetscReal)itq/(PetscReal)its));CHKERRQ(ierr);
    ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
    ierr = PetscLogStagePop();CHKERRQ(ierr);
  }

  for (variant=1; variant<4; variant++) {
    ierr = MatDestroy(&mats[variant]);CHKERRQ(ierr);
  }
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&rhsnest);CHKERRQ(ierr);
  ierr = VecDestroy(&sol);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

